# Introduction

Hi there. My name is Ruby and I'm a software developer with ADHD. 

Over the past few years I've been customizing and refining my workflow in order
to create a development environment that works with my brain rather than forcing
my brain to conform to existing solutions. 

For a while I've been considering writing an article on Medium or Dev.to to
put some of my ideas out there to hopefully help other devs. The more I've
thought about it, however, the more I've realized that it would be helpful to
have a place where we can pool our resources together and create a permanent and
living reference to return to. 


## Why "Bees in my Head"? 

When trying to come up with a name I was reminded of 
[this reddit thread](https://www.reddit.com/r/adhdmeme/comments/mhx6is/bees_in_my_head/), 
which really seems to have struck a cord with a lot of people, myself included.
It's a lighthearted, yet fairly accurate description of how I often feel, so
here we are. 

## Goals

I primarily work with Javascript on VIM in a Linux environment, so this is
primarily the perspective I will be approaching things from, however I want this
site to be:

- Language agnostic
- OS Agnostic
- Welcome to all, regardless of gender, race, ethnicity, religious persuasion,
  sexual identity/orientation, favorite football team, or any other attribute
  used to separate "us" from "them". 
- A place to help each other with being more effective software developers. 

If you're a 95 year old trans zoroastrian with tips on writing 
[Brainfuck](https://esolangs.org/wiki/Brainfuck) on  [Temple OS](https://en.wikipedia.org/wiki/TempleOS), 
your contributions are welcome. 

I'm at the very beginning stages right now and what you see is pretty much what
exists, but for those interested in contributing, the site is built on vuepress
and hosted on a public [gitlab
repo](https://gitlab.com/adhd-dev/bees-in-my-head). For the moment I want to
keep things as simple as possible in order to prevent myself from going down
unnecessary rabbit holes. 

Finally, I want to make a note of some of the topics I plan to cover in my first
articles:

- Using TMUX and a window manager to minimize disruption to your workflow caused
  by conext switching.
- Using fuzzy searching wherever possible to get what/where you want as easily
  as possible.
- Creating a file structure optimized for search. 
- VIM plugins & techniques I find helpful. 
- Useful bash aliases & small utilities that improve your workflow. 

Also, I'd like to create a section dedicated to resources for employers, loved
ones, and co-workers to help them understand the challenges we face and
hopefully provide context that can help them get a better understanding on how
to work more effectively with us.
  
I have some ideas that I think could be very helpful to others, but I'm really
excited to learn what other people are doing to be happier and more effective. 
