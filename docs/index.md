---
home: true
heroImage: /beehive.svg
heroText: Bees in my Head
actionLink: /blog/
actionText: Enter
tagline: Tips, tricks, and resources for software developers with executive function issues. 
features:
- title: Tools
  details: Discover & share tools that reduce friction between you and your code. 
- title: Open community
  details: We are language agnostic, OS agnostic, and welcoming to all who want to take part. 
- title: Resources
  details: Share external resources for those with ADHD and the people who live & work with us. 
---

