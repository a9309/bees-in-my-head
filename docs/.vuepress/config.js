module.exports = {
  title: "Bees in my Head", // The website title is used as a prefix for all page titles. By default, it will be displayed on the navigation bar (navbar)
  description: "A place for software developers with executive function", // The description of the website, which will be rendered into the HTML of the current page with < meta > tags
  dest: "public", // Specify the output directory of vuepress build.
  base: "/", // The basic path of the deployment site. If you want to deploy your website to a sub path, you will need to set it.
  head: [
    // The tag injected into the HTML < head > of the current page
    ["link", { rel: "icon", href: "/images/favicon.ico" }],
    ["link", { rel: "manifest", href: "/mainfest.json" }],
    ["meta", { name: "theme-color", content: "#8e7b47" }],
    /*    ["meta", { name: "apple-mobile-web-app-capable", content: "yes" }],*/
    /*[*/
    /*"meta",*/
    /*{ name: "apple-mobile-web-app-status-bar-style", content: "black" },*/
    /*],*/
    /*[*/
    /*"link",*/
    /*{ rel: "apple-touch-icon", href: `/icons/apple-touch-icon-152x152.png` },*/
    /*],*/
    /*[*/
    /*"link",*/
    /*{*/
    /*rel: "mask-icon",*/
    /*href: "/icons/safari-pinned-tab.svg",*/
    /*color: "#3eaf7c",*/
    /*},*/
    /*],*/
    /*[*/
    /*"meta",*/
    /*{*/
    /*name: "msapplication-TileImage",*/
    /*content: "/icons/msapplication-icon-144x144.png",*/
    /*},*/
    /*],*/
    /*["meta", { name: "msapplication-TileColor", content: "#000000" }],*/
  ],
  serviceWorker: true, // Is PWA enabled
  theme: "yuu",
  themeConfig: {
    yuu: {
      defaultDarkTheme: true,
    },
    // Theme configuration. The default theme is used here, and other themes can be switched
    logo: "/beehive.svg",
    repo: "https://gitlab.com/adhd-dev/bees-in-my-head", // github warehouse
    editLinks: true, // Show edit this page button in footer
    docsDir: "docs", // github edit path
    docsBranch: "main",
    smoothScroll: true,
    displayAllHeaders: true, // Show Title links for all pages
    sidebar: "auto", // Sidebar configuration
    sidebarDepth: 2, // The sidebar shows level 2
  },
};
